FROM gcr.io/distroless/cc-debian11

WORKDIR /app

COPY target/caicai-1.0.0-SNAPSHOT-runner /app

EXPOSE 80

CMD ["/app/caicai-1.0.0-SNAPSHOT-runner"]